package com.zzoran.currencyconverter.calculator.mvp;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.zzoran.currencyconverter.common.data.CurrencyItem;

/**
 * Calculator screen presenter.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public interface CalcPresenter extends MvpPresenter<CalcView> {

    /**
     * Method for calculating result.
     * @param from From currency.
     * @param to To currency.
     * @param amount Money amount to convert from.
     */
    public void calculateResult(CurrencyItem from, CurrencyItem to, String amount);

    /**
     * Load currency data in screen.
     */
    public void loadData();
}
