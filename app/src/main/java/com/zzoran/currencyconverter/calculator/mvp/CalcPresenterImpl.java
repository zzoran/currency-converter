package com.zzoran.currencyconverter.calculator.mvp;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.zzoran.currencyconverter.common.async.CurrencyFetcher;
import com.zzoran.currencyconverter.common.async.ResponseListener;
import com.zzoran.currencyconverter.common.data.CurrencyItem;
import com.zzoran.currencyconverter.common.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Implementation of Calculator presenter.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class CalcPresenterImpl extends MvpBasePresenter<CalcView> implements CalcPresenter {

    @Override
    public void calculateResult(CurrencyItem from, CurrencyItem to, String amountStr) {
        int textId = getView().getValidationErrorMessage();
        if(textId!=-1){
            getView().showMessage(textId);
            return;
        }
        try {
            float amount = Float.valueOf(amountStr);
            float result = ((amount / from.getUnitValue()) * Float.valueOf(from.getMedianRate())) * (to.getUnitValue() / Float.valueOf(to.getMedianRate()));
            getView().showContent();
            getView().displayResult(result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void loadData() {
        getView().showLoading(false);
        String currDate = new SimpleDateFormat(Constants.REQUEST_DATE_FORMAT).format(new Date());
        CurrencyFetcher.getCurrenciesByDate(currDate, new ResponseListener<List<CurrencyItem>>() {
            @Override
            public void onData(List<CurrencyItem> data) {
                if(data==null || data.size()==0){
                    getView().showError(null, false);
                }else{
                    getView().showContent();
                    getView().setData(data);
                }
            }

            @Override
            public void onError(Throwable err) {
                getView().showError(err, false);
            }
        });
    }
}
