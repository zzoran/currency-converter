package com.zzoran.currencyconverter.calculator;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.zzoran.currencyconverter.R;
import com.zzoran.currencyconverter.calculator.mvp.CalcPresenterImpl;
import com.zzoran.currencyconverter.calculator.mvp.CalcView;
import com.zzoran.currencyconverter.common.data.CurrencyItem;
import com.zzoran.currencyconverter.common.utils.Constants;
import com.zzoran.currencyconverter.common.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnItemSelected;
import butterknife.Unbinder;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Screen where user can calculate currency conversion.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class CalculatorFragment extends MvpLceFragment<LinearLayout, List<CurrencyItem>, CalcView, CalcPresenterImpl> implements CalcView{

    // views
    @BindView(R.id.spinner_currency_from) Spinner mSpinnerFrom;
    @BindView(R.id.spinner_currency_to) Spinner mSpinnerTo;
    @BindView(R.id.editText_amount) EditText mEditTextAmount;
    @BindView(R.id.button_calculate) Button mButtonCalculate;
    @BindView(R.id.text_calculation_result) TextView mTextCalculationResult;
    private Unbinder unbinder;

    // data
    private List<CurrencyItem> data;

    public static CalculatorFragment newInstance(){
        CalculatorFragment f = new CalculatorFragment();
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calculator, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @OnItemSelected({R.id.spinner_currency_to, R.id.spinner_currency_from})
    protected void onSpinnerToSelectedItem(int position){
        clearResult();
    }

    @OnEditorAction(R.id.editText_amount)
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onCalculateClick();
            return true;
        }
        return false;
    }

    @OnClick(R.id.button_calculate)
    protected void onCalculateClick(){
        CurrencyItem from = (CurrencyItem)mSpinnerFrom.getSelectedItem();
        CurrencyItem to = (CurrencyItem)mSpinnerTo.getSelectedItem();
        String amount = mEditTextAmount.getText().toString();
        presenter.calculateResult(from, to, amount);
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public CalcPresenterImpl createPresenter() {
        return new CalcPresenterImpl();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData(false);
    }

    /**
     * Method returns -1 if validation is successfully invoked. Otherwise it returns message string resource id.
     * @return
     */
    @Override
    public int getValidationErrorMessage(){
        if(mEditTextAmount!=null) {
            String txt = mEditTextAmount.getText().toString();
            if (TextUtils.isEmpty(txt)) {
                return R.string.msg_please_set_amount;
            }
            try {
                float amount = Float.parseFloat(txt);
                if (amount <= 0) {
                    return R.string.msg_please_set_amount;
                }
            } catch (NumberFormatException e) {
                return R.string.msg_please_set_amount;
            }
        }
        if(mSpinnerFrom!=null && mSpinnerTo!=null){
            CurrencyItem ciFrom = (CurrencyItem)mSpinnerFrom.getSelectedItem();
            CurrencyItem ciTo = (CurrencyItem)mSpinnerTo.getSelectedItem();
            if(ciFrom!=null && ciTo!=null && (ciFrom.getCurrencyCode().equalsIgnoreCase(ciTo.getCurrencyCode()))){
                return R.string.msg_select_different_from_to_currencies;
            }
        }
        return -1;
    }

    @Override
    public void displayResult(float value) {
        mTextCalculationResult.setText( String.format("%.2f", value));
        dismissKeyboard();
    }

    private void dismissKeyboard(){
        if(getActivity()==null){
            return;
        }
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            if (imm.isAcceptingText()) {
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void clearResult(){
        if(mTextCalculationResult!=null){
            mTextCalculationResult.setText("");
        }
    }

    @Override
    public void showMessage(int stringId) {
        if(getActivity()!=null) {
            Toast.makeText(getActivity(), stringId, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        Utils.displayNoInternetConnectionDialog(getActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadData(false);
            }
        });
        return getString(R.string.alert_no_data_message);
    }

    @Override
    public void setData(List<CurrencyItem> data) {
        if(data==null || data.size()==0 || mSpinnerFrom==null || mSpinnerTo==null || getActivity()==null){
            return;
        }
        this.data = data;

        // put default currency that is not returned in API response.
        CurrencyItem ciHrk = new CurrencyItem(Constants.DEFAULT_CURRENCY_UNIT_VALUE, Constants.DEFAULT_CURRENCY, "1", "1", "1");
        data.add(0, ciHrk);

        ArrayAdapter<CurrencyItem> adapter = new ArrayAdapter<CurrencyItem>(getActivity(), android.R.layout.simple_spinner_dropdown_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSpinnerFrom.setAdapter(adapter);
        mSpinnerTo.setAdapter(adapter);
        if(data.size()>1) {
            mSpinnerTo.setSelection(1); // prevent selecting the same currency as 'from' and 'to'
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadData();
    }

}
