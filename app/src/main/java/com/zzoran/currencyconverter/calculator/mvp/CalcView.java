package com.zzoran.currencyconverter.calculator.mvp;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.zzoran.currencyconverter.common.data.CurrencyItem;

import java.util.List;

/**
 * Calculator view.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public interface CalcView extends MvpLceView<List<CurrencyItem>>{

    /**
     * Removes result value from result label.
     */
    public void clearResult();

    /**
     * Displays message dialog.
     * @param stringId String resource ID.
     */
    public void showMessage(int stringId);

    /**
     * Returns error string ID resource.
     * @return -1 if validation didn't found any problem, otherwise it returns error string ID.
     */
    public int getValidationErrorMessage();

    /**
     * Display conversion result.
     * @param value Money amount to convert.
     */
    public void displayResult(float value);

}
