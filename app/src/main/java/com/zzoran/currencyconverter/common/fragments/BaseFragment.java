package com.zzoran.currencyconverter.common.fragments;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.zzoran.currencyconverter.common.utils.Utils;


/**
 * Base fragment that contains progress views that share all fragments.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public abstract class BaseFragment extends Fragment {

    private ProgressBar mProgressBar;
    private TextView mTextEmpty;
    private LinearLayout mContentLayout;

    public abstract void loadData();

    public void initViews(View parent, int contentLayoutId, int progressBarId, int txtEmptyId){
        mContentLayout = (LinearLayout)parent.findViewById(contentLayoutId);
        mTextEmpty = (TextView)parent.findViewById(txtEmptyId);
        mProgressBar = (ProgressBar)parent.findViewById(progressBarId);
    }

    protected void displayProgress(boolean visible){
        if(mProgressBar==null || mContentLayout==null || mTextEmpty==null){
            return;
        }
        mProgressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
        mContentLayout.setVisibility(visible ? View.GONE : View.VISIBLE);
        mTextEmpty.setVisibility(View.GONE);
    }

    protected void displayEmptyText(){
        if(mProgressBar==null || mContentLayout==null || mTextEmpty==null){
            return;
        }
        mTextEmpty.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mContentLayout.setVisibility(View.GONE);
        Utils.displayNoInternetConnectionDialog(getActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadData();
            }
        });
    }

    protected void showMessage(final String msg){
        if(getActivity()!=null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

}
