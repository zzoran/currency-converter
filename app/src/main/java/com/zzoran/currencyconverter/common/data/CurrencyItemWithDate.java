package com.zzoran.currencyconverter.common.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Currency data class that extends {@link CurrencyItem} with additional property.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class CurrencyItemWithDate  extends CurrencyItem{

    @SerializedName("date")
    private Date date;

    public Date getDate() {
        return date;
    }
}
