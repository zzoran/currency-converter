package com.zzoran.currencyconverter.common.utils;

/**
 * Constants.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class Constants {

    public static final String DEFAULT_CURRENCY = "HRK";
    public static final int DEFAULT_CURRENCY_UNIT_VALUE = 1;
    public static final String REQUEST_DATE_FORMAT = "yyyy-MM-dd";
    public static final String GRAPH_SMALL_DATE_FORMAT = "dd.MM.";
}
