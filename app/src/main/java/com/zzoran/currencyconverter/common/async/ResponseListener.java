package com.zzoran.currencyconverter.common.async;

/**
 * Generic response listener.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public interface ResponseListener<T> {
    public void onData(T data);
    public void onError(Throwable err);
}
