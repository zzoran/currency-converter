package com.zzoran.currencyconverter.common.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.zzoran.currencyconverter.R;


/**
 * Various utility methods.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class Utils {

    /**
     * Method displays dialog with appropriate message when there isn't provided internet connection.
     * @param activity Activity context.
     * @param okListener OK button listener.
     */
    public static void displayNoInternetConnectionDialog(Activity activity, DialogInterface.OnClickListener okListener){
        if(activity==null){
            return;
        }
        new AlertDialog.Builder(activity)
                .setTitle(R.string.alert_no_data_message_title)
                .setMessage(R.string.alert_no_data_message)
                .setNegativeButton(R.string.alert_no_data_cancel_bttn, null)
                .setPositiveButton(R.string.alert_no_data_retry_bttn, okListener)
                .create().show();
    }
}
