package com.zzoran.currencyconverter.common.async;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zzoran.currencyconverter.common.data.CurrencyItem;
import com.zzoran.currencyconverter.common.data.CurrencyItemWithDate;
import com.zzoran.currencyconverter.common.utils.Constants;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Main class responsible for fetching currency data.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class CurrencyFetcher {

    private static final String TAG = "request";

    private static CurrencyFetcher sCurrencyFetcher = new CurrencyFetcher();
    private static String mBaseUrl;
    private static CurrencyService service;

    public interface CurrencyService {
        @GET("api/v1/rates/daily/")
        Call<List<CurrencyItem>> listCurrencies(@Query("date") String date);

        @GET("api/v1/rates/{currency}/")
        Call<List<CurrencyItemWithDate>> getCurrencyHistoryData(@Path("currency") String currency, @Query("from") String from, @Query("to") String to);
    }

    public CurrencyFetcher(){
    }

    public static void init(String baseUrl){
        mBaseUrl = baseUrl;
        service = createService();
    }

    public static void getCurrenciesByDate(String date, final ResponseListener<List<CurrencyItem>> listener){
        Call<List<CurrencyItem>> currencyList = service.listCurrencies(date);
        currencyList.enqueue(new Callback<List<CurrencyItem>>() {
            @Override
            public void onResponse(Call<List<CurrencyItem>> call, Response<List<CurrencyItem>> response) {
                if(listener!=null){
                    listener.onData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<CurrencyItem>> call, Throwable t) {
                if(listener!=null){
                    listener.onError(t);
                }
            }
        });
    }

    public static void getCurrencyHistoryData(String currency, String dateFrom, String dateTo, final ResponseListener<List<CurrencyItemWithDate>> listener){
        Call<List<CurrencyItemWithDate>> currencyList = service.getCurrencyHistoryData(currency, dateFrom, dateTo);
        currencyList.enqueue(new Callback<List<CurrencyItemWithDate>>() {
            @Override
            public void onResponse(Call<List<CurrencyItemWithDate>> call, Response<List<CurrencyItemWithDate>> response) {
                if(listener!=null){
                    listener.onData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<CurrencyItemWithDate>> call, Throwable t) {
                if(listener!=null){
                    listener.onError(t);
                }
            }
        });
    }

    private static CurrencyService createService(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor())
                .build();

        Gson gson = new GsonBuilder()
                .setDateFormat(Constants.REQUEST_DATE_FORMAT)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(sCurrencyFetcher.mBaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        CurrencyService service = retrofit.create(CurrencyService.class);
        return service;
    }

    private static class LoggingInterceptor implements Interceptor {
        @Override public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            long t1 = System.nanoTime();
            Log.i(TAG,String.format("Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers()));

            okhttp3.Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            Log.i(TAG,String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            return response;
        }
    }



}
