package com.zzoran.currencyconverter.common.data;

import com.google.gson.annotations.SerializedName;

/**
 * Currency data class.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class CurrencyItem {

    @SerializedName("unit_value")
    private int unitValue;

    @SerializedName("currency_code")
    private String currencyCode;

    @SerializedName("median_rate")
    private String medianRate;

    @SerializedName("buying_rate")
    private String buyingRate;

    @SerializedName("selling_rate")
    private String sellingRate;

    public CurrencyItem(){
        super();
    }

    public CurrencyItem(int unitValue, String currencyCode, String medianRate, String buyingRate, String sellingRate) {
        this.unitValue = unitValue;
        this.currencyCode = currencyCode;
        this.medianRate = medianRate;
        this.buyingRate = buyingRate;
        this.sellingRate = sellingRate;
    }

    public int getUnitValue() {
        return unitValue;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getMedianRate() {
        return medianRate;
    }

    public String getBuyingRate() {
        return buyingRate;
    }

    public String getSellingRate() {
        return sellingRate;
    }

    @Override
    public String toString() {
        return currencyCode;
    }
}
