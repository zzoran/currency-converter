package com.zzoran.currencyconverter;

import android.app.Application;

import com.zzoran.currencyconverter.common.async.CurrencyFetcher;

/**
 * Main application class.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class App extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        CurrencyFetcher.init(getString(R.string.service_url));
    }
}
