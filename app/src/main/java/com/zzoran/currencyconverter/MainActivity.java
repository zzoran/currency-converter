package com.zzoran.currencyconverter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.zzoran.currencyconverter.calculator.CalculatorFragment;

/**
 * Main activity that displays currency converter screen.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState==null){
            // preload first fragment
            replaceFragment(CalculatorFragment.newInstance());
        }
    }

    /**
     * Method for changing fragment.
     * @param f
     */
    private void replaceFragment(Fragment f){
        if(f==null){
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.content_container, f).commit();
    }
}
