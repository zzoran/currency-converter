package com.zzoran.currencyconverter;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.zzoran.currencyconverter.common.data.CurrencyItem;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Test for checking if calculation is valid.
 *
 * @author Zoran Sasko
 * @version 1.0
 */
@RunWith(AndroidJUnit4.class)
public class CalculatorScreenTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void calculatorScreenTest() {
        onView(withId(R.id.editText_amount)).perform(replaceText("123"), closeSoftKeyboard());

        ViewInteraction appCompatSpinner = onView(withId(R.id.spinner_currency_from));
        appCompatSpinner.perform(click());

        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(android.R.id.text1), withText("HUF"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        onView(withId(R.id.spinner_currency_to)).perform(click());
        onData(allOf(is(instanceOf(CurrencyItem.class)), withCurrencyItemValue("JPY"))).perform(click());


        ViewInteraction appCompatButton = onView(withId(R.id.button_calculate));
        appCompatButton.perform(click());

        onView(withId(R.id.text_calculation_result)).check(matches(withText("48.26")));

    }

    public static <T> Matcher<T> withCurrencyItemValue(final String name) {
        return new BaseMatcher<T>() {
            @Override
            public boolean matches(Object item) {
                if(item instanceof CurrencyItem){
                    CurrencyItem i = (CurrencyItem)item;
                    return i.getCurrencyCode().equalsIgnoreCase(name);
                }
                return false;
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
